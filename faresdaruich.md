<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Project web</title>
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@400;600&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="styles.css">
</head>
<body>
    <div class="wrapper">
        <!-- nav starts -->
        <nav class="navContent container">
            <p class="logo">designey</p> 
            <ul class="headerLinks container">
                <li><a href="#about">our practice</a></li>
                <li><a href="#projects">Projects</a></li>
                <li><a href="#contact">Contact</a></li>
                <li><a href="#boom">feedback</a></li>
                
            </ul>
            
        </nav>
        <!-- nav ends-->

        <!-- header begins -->
        <header>
            <div class="headerContent container">
                <h1>Designey is an architecture studio </h1>
                <div class="header-assets">
                    <div class="badge">
                        <img src="./static/badge.png" alt="Badge">
                    </div>
                    <div class="headerImg">
                        <img src="./static/header.jpg" alt="Living room">
                    </div>
                </div>
            </div>
        </header>
        <!-- header ends -->
    </div>
   <main>
        <div class="wrapper">
            
            <section class="practiceContent container" id="about">
                <div class="practiceText">
                    <h2><span>Our practice</span> </h2>
                    <p>The practice of architecture consists of the provision of professional services in connection with town planning as well as the design, construction, enlargement, conservation, restoration, or alteration of a building or group of buildings. These professional services include, but are not limited to: planning and land-use planning,provision of preliminary studies, designs, models, drawings and specifications,monitoring of construction and project management .</p>
                    <button><a href="#contact">Send Message</a></button>
                </div>
                <div class="practice-assets">
                    <div class="practiceImg1">
                        <img src="./static/image1.jpg" alt="Kitchen">
                    </div>
                    <div class="practiceImg2">
                        <img src="./static/image2.jpg" alt="Kitchen2">
                    </div>
                </div>
            </section>
    </main>
 </body>
 </html>